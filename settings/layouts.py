from libqtile import layout

layouts = [
    layout.Columns(
        border_focus_stack=['#d75f5f', '#8f3d3d'],
        border_width=4
    ),
    layout.Max(),
    layout.Tile(),
    layout.Stack(num_stacks=2),
    layout.Bsp(),
    layout.Matrix(),
    layout.MonadTall(),
    layout.MonadWide(),
    layout.RatioTile(),
    layout.TreeTab(),
    layout.VerticalTile(),
    layout.Zoomy(),
]

